# ImagesSubRedditViewer

ImagesSubRedditViewer is an unofficial app for browsing a specific subreddit /r/images.
The app will load up the subreddit url, and display all the posts as a list of images in the UI.
For the assignment, the app will be split into two, :app and :ima

# Get Complete Code from master branch

# Features
1. A library to load the images from the server

# Components Used
1. A clean MVVM pattern architecture
2. Implemented Koin Component with scope
3. Used Response Handler to handle network failures.
4. Binding Adapter to bind the data
5. Used Androidx component
6. Data Binding concept included
7. Seperated based on View, viewModel, Usecase, Repository, RepositoryImplementation to handle the network calls and logics.