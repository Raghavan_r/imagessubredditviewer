package imageviewer.com.imagessubredditviewer.repo

import imageviewer.com.imagessubredditviewer.networking.Resource
import imageviewer.com.imagessubredditviewer.reponse.ImageListResponse

/**
 * Repo which used to make network call to get imagelist
 * */
interface ImageListRepo {

    suspend fun getImageList() : Resource<ImageListResponse>
}