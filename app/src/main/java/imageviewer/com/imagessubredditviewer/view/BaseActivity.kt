package imageviewer.com.imagessubredditviewer.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import imageviewer.com.imagessubredditviewer.R
import imageviewer.com.imagessubredditviewer.di.IMAGE_LIST_SCOPE_ID
import imageviewer.com.imagessubredditviewer.di.IMAGE_LIST_SCOPE_QUALIFIER
import kotlinx.coroutines.*
import org.koin.core.scope.Scope
import org.koin.java.KoinJavaComponent.getKoin
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Abstract Activity that has a coroutine scope that lives between
 */
abstract class BaseActivity(
        private val parentContext: CoroutineContext = EmptyCoroutineContext
) : AppCompatActivity() {

    protected lateinit var coroutineScope: CoroutineScope
        private set

    val viewModelScope: Scope? get() = _currentScope

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope =
                CoroutineScope(Dispatchers.Main + parentContext + SupervisorJob(parentContext[Job]))
    }

    override fun onDestroy() {
        coroutineScope.cancel()
        super.onDestroy()
    }

    private fun internalCreateScope(): Scope? {
        val created = getKoin().getScopeOrNull(IMAGE_LIST_SCOPE_ID) == null
        val scope = getKoin()
                .getOrCreateScope(IMAGE_LIST_SCOPE_ID, IMAGE_LIST_SCOPE_QUALIFIER)

        if (created) {
            scope.declare(this)
        }

        return scope
    }

    private var _currentScope = internalCreateScope()


    protected fun showErrorMessage(message: String, view: View) {
        val snack = Snackbar.make(
                view,
                message,
                Snackbar.LENGTH_INDEFINITE
        )
        snack.setAction(resources.getString(R.string.ok)) {
            snack.dismiss()
        }
        snack.show()
    }

}