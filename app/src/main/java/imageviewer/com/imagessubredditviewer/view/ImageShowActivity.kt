package imageviewer.com.imagessubredditviewer.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.imageloader.main.ImageLoader
import imageviewer.com.imagessubredditviewer.R
import imageviewer.com.imagessubredditviewer.databinding.ActivityImageShowBinding
import imageviewer.com.imagessubredditviewer.util.CACHE_SIZE
import imageviewer.com.imagessubredditviewer.viewmodel.ImageListViewModel
import org.koin.android.viewmodel.ext.android.viewModel


class ImageShowActivity : BaseActivity() {
    private val viewModel: ImageListViewModel by viewModelScope?.viewModel(this)
    private lateinit var imageLoader: ImageLoader
    private lateinit var  binding : ActivityImageShowBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityImageShowBinding>(this,R.layout.activity_image_show)
        binding.viewModel = viewModel
        val url = intent.getStringExtra("url")
        url?.let {
            loadImage(url)
        }
    }

    private fun loadImage(url: String) {
        imageLoader = ImageLoader.getInstance(this, CACHE_SIZE)
        imageLoader.displayImage(url,binding.ivImage,R.drawable.placeholder)
    }
}
