package imageviewer.com.imagessubredditviewer.view

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import imageviewer.com.imagessubredditviewer.R
import imageviewer.com.imagessubredditviewer.adapter.ImageListAdapter
import imageviewer.com.imagessubredditviewer.databinding.ActivityImageListBinding
import imageviewer.com.imagessubredditviewer.listeners.OnAdapterItemClickListener
import imageviewer.com.imagessubredditviewer.viewmodel.ImageListViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ImageListActivity : BaseActivity(),OnAdapterItemClickListener {

    private val viewModel: ImageListViewModel by viewModelScope?.viewModel(this)
    private var adapter = ImageListAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityImageListBinding>(this,R.layout.activity_image_list)
        binding.viewModel = viewModel
        binding.adapter = adapter

        viewModel.getImageList()

        viewModel.imageListResponse.observe(this, Observer {
            adapter.setDataList(it ?: emptyList())
        })

        viewModel.networkError.observe(this, Observer {
            showErrorMessage(it,binding.root)
        })
    }

    override fun onItemClick(url: String) {
        val intent = Intent(this,ImageShowActivity::class.java)
        intent.putExtra("url",url)
        startActivity(intent)
    }
}
