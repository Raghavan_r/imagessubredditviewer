package imageviewer.com.imagessubredditviewer.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import imageviewer.com.imagessubredditviewer.R
import imageviewer.com.imagessubredditviewer.databinding.ActivitySplashBinding
import imageviewer.com.imagessubredditviewer.util.DELAY_TIME
import imageviewer.com.imagessubredditviewer.viewmodel.ImageListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class SplashActivity : BaseActivity() {

    private val viewModel: ImageListViewModel by viewModelScope?.viewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivitySplashBinding>(this,R.layout.activity_splash)
        binding.viewModel = viewModel
        navigate()

    }

    private fun navigate() {
        GlobalScope.launch(Dispatchers.Main.immediate) {
            delay(DELAY_TIME.toLong())
            startActivity(viewModel.navigate())
            finish()
        }
    }
}
