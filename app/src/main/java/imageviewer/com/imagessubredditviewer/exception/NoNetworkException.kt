package imageviewer.com.imagessubredditviewer.exception

import java.io.IOException

/**
 * Custom exception class which will let the user if no internet available
 * */
open class NoNetworkException : IOException() {

    override fun getLocalizedMessage(): String? = "No Internet Connection"
}