package imageviewer.com.imagessubredditviewer.viewmodel

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import imageviewer.com.imagessubredditviewer.reponse.Children
import imageviewer.com.imagessubredditviewer.usecase.ImageListUseCase
import imageviewer.com.imagessubredditviewer.view.ImageListActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ImageListViewModel(private val context: Context,
                         private val imageUseCase: ImageListUseCase) : ViewModel() {

    private val _networkError = MutableLiveData<String>()
    val networkError: LiveData<String> = _networkError

    private val _imageListResponse = ArrayList<Children>()
    val imageListResponse = MutableLiveData<List<Children>>()

    fun getImageList() {

        viewModelScope.launch(Dispatchers.Main.immediate) {
            try {
                imageUseCase.getImageList()?.let {
                response ->
                    _imageListResponse.addAll(response.data.children)
                    imageListResponse.value = _imageListResponse
                }
            } catch (e: Exception) {
                _networkError.value = e.localizedMessage
            }

        }
    }


    fun navigate() : Intent =
        Intent(context, ImageListActivity::class.java)



}