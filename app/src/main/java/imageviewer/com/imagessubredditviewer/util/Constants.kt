package imageviewer.com.imagessubredditviewer.util

const val SERVER_URL = "https://www.reddit.com"
const val TIME_OUT = 60L
const val DELAY_TIME = 3000
const val CACHE_SIZE = 12*1024*1024