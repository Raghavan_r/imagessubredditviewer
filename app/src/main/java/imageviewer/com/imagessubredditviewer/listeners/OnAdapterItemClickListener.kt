package imageviewer.com.imagessubredditviewer.listeners

/**
 * Adapter click listener for Recyclerview adapters
 * */
interface OnAdapterItemClickListener {

    /**
     * This method is invoked when the adapter item is clicked
     * */
    fun onItemClick(url: String)
}