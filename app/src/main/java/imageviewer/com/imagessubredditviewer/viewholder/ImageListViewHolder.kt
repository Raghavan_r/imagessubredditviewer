package imageviewer.com.imagessubredditviewer.viewholder

import androidx.recyclerview.widget.RecyclerView
import imageviewer.com.imagessubredditviewer.BR
import imageviewer.com.imagessubredditviewer.databinding.RowImageListBinding
import imageviewer.com.imagessubredditviewer.listeners.OnAdapterItemClickListener
import imageviewer.com.imagessubredditviewer.reponse.Children


class ImageListViewHolder(private val binding: RowImageListBinding)
    : RecyclerView.ViewHolder(binding.root) {
    fun bind(children: Children, onAdapterItemClickListener: OnAdapterItemClickListener) {
        binding.setVariable(BR.imageList, children.data.thumbnail)
        binding.executePendingBindings()

        binding.ivImage.setOnClickListener {
            onAdapterItemClickListener.onItemClick(children.data.url)
        }
    }

}
