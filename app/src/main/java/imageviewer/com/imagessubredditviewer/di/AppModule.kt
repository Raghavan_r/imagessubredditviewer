package imageviewer.com.imagessubredditviewer.di

import androidx.fragment.app.DialogFragment
import imageviewer.com.imagessubredditviewer.networking.ResponseHandler
import imageviewer.com.imagessubredditviewer.networking.remoteDataSourceModule
import imageviewer.com.imagessubredditviewer.repo.ImageListRepo
import imageviewer.com.imagessubredditviewer.repoimpl.ImageListRepoImpl
import imageviewer.com.imagessubredditviewer.usecase.ImageListUseCase
import imageviewer.com.imagessubredditviewer.viewmodel.ImageListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import org.koin.dsl.module

/**
 * ScopeID for the current logged in scope
 */
const val IMAGE_LIST_SCOPE_ID = "LOGGED_IN"

/**
 * Qualifier for the logged in scope
 */
val IMAGE_LIST_SCOPE_QUALIFIER = named(IMAGE_LIST_SCOPE_ID)

/**
 * App Components
 */
val readerAppModule = module {

    scope(IMAGE_LIST_SCOPE_QUALIFIER) {

        viewModel { ImageListViewModel(get(),get()) }

    }
}

val useCaseModule = module {
    scope(IMAGE_LIST_SCOPE_QUALIFIER) {
        scoped {
            ImageListUseCase(get())
        }
    }
}

val constantModules = module {
    scope(IMAGE_LIST_SCOPE_QUALIFIER) {
        scoped {
            ResponseHandler()
        }
    }
}

val repositoryModule = module {

    scope(IMAGE_LIST_SCOPE_QUALIFIER) {
        scoped<ImageListRepo> {
            ImageListRepoImpl(get(),get())
        }

    }
}


// Gather all app modules
val imageReaderApp = listOf(
        readerAppModule,remoteDataSourceModule,
        constantModules, useCaseModule, repositoryModule
)