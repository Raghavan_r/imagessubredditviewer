package imageviewer.com.imagessubredditviewer.usecase

import imageviewer.com.imagessubredditviewer.exception.ApiFailureException
import imageviewer.com.imagessubredditviewer.networking.Status
import imageviewer.com.imagessubredditviewer.repo.ImageListRepo
import imageviewer.com.imagessubredditviewer.reponse.ImageListResponse

/**
 * Use case class which contains all the logics
 *
* **/
class ImageListUseCase(private val imageListRepo: ImageListRepo) {

    @Throws(ApiFailureException::class)
    suspend fun getImageList(): ImageListResponse? {
        imageListRepo.getImageList().apply {
            return when (this.status) {
                Status.SUCCESS -> this.data
                Status.ERROR -> throw ApiFailureException(this.message)
                else -> null
            }

        }

    }
}
