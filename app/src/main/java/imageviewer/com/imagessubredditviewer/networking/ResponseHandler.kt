package imageviewer.com.imagessubredditviewer.networking

import imageviewer.com.imagessubredditviewer.exception.NoNetworkException
import org.json.JSONObject
import retrofit2.HttpException

private const val INVALID_TOKEN = 401

/**
 * Handle the Network call process safely
 * Handle all the Network level issues
 * */
open class ResponseHandler {

    /**
     * Method used when the network call is success
     * */
    fun <T : Any> handleSuccess(data: T): Resource<T> = Resource.success(data)

    /**
     *Method called when any exception occur during network call
     */

    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is HttpException -> {
                val message = if (e.code() == INVALID_TOKEN) {
                    "Token expired"
                } else {
                    getError(e)
                }

                Resource.error(message, null)
            }

            is NoNetworkException -> {
                Resource.error(e.localizedMessage, null)
            }

            else -> {
                Resource.error(e.localizedMessage, null)
            }
        }
    }

    private fun getError(e: Exception): String? {
        return try {
            val jObjError = JSONObject((e as HttpException).response()?.errorBody()?.string())
            jObjError.getString("error")
        } catch (e: Exception) {
            "Server error"
        }
    }
}