package imageviewer.com.imagessubredditviewer.networking

import imageviewer.com.imagessubredditviewer.reponse.ImageListResponse
import retrofit2.http.GET

/**
 * Class usually contains all the end point Url
 * */
interface UserDataSource {

@GET("/r/images/hot.json")
suspend fun getImageList() : ImageListResponse

}