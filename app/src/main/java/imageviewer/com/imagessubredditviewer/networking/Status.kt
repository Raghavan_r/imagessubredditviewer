package imageviewer.com.imagessubredditviewer.networking

/**
 * An enum class which holds the key for network status
 * */
enum class Status {
        SUCCESS,
        ERROR,
        LOADING
}
