package imageviewer.com.imagessubredditviewer.repoimpl

import imageviewer.com.imagessubredditviewer.networking.Resource
import imageviewer.com.imagessubredditviewer.networking.ResponseHandler
import imageviewer.com.imagessubredditviewer.networking.UserDataSource
import imageviewer.com.imagessubredditviewer.repo.ImageListRepo
import imageviewer.com.imagessubredditviewer.reponse.ImageListResponse

/**
 * Implementation class of [ImageListRepo]
 * */
class ImageListRepoImpl(private val responseHandler: ResponseHandler,
                        private val userDataSource: UserDataSource) : ImageListRepo {

    override suspend fun getImageList(): Resource<ImageListResponse> {
        return try {
            val response = userDataSource.getImageList()
            responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }

    }

}