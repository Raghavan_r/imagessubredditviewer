package imageviewer.com.imagessubredditviewer.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import imageviewer.com.imagessubredditviewer.databinding.RowImageListBinding
import imageviewer.com.imagessubredditviewer.listeners.OnAdapterItemClickListener
import imageviewer.com.imagessubredditviewer.reponse.Children
import imageviewer.com.imagessubredditviewer.viewholder.ImageListViewHolder

class ImageListAdapter (private val onAdapterItemClickListener: OnAdapterItemClickListener) :
        RecyclerView.Adapter<ImageListViewHolder>() {

    private var imageList: List<Children> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = RowImageListBinding.inflate(layoutInflater, parent, false)
        return ImageListViewHolder(
                itemBinding
        )
    }

    override fun onBindViewHolder(holder: ImageListViewHolder, position: Int) {
        holder.bind(imageList[position], onAdapterItemClickListener)
    }

    /**
     * Used to set the data to the adapter
     * @param [items] is the updated student list from the viewModel
     */
    fun setDataList(items: List<Children>) {
        this.imageList = items
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = imageList.size
}
