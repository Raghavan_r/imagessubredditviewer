package imageviewer.com.imagessubredditviewer.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.example.imageloader.main.ImageLoader
import imageviewer.com.imagessubredditviewer.R
import imageviewer.com.imagessubredditviewer.util.CACHE_SIZE

/**
 * Binding adapter class for binding the adapter image
 * */
object ImageBindingAdapter {

    /**
     * To show image from url
     * @param imageView
     * @param imageUrl
     * @param placeholder
     */
    @JvmStatic
    @BindingAdapter(value = ["android:src"], requireAll = false)
    fun setImageUrl(view: ImageView, url: String) {
        val imageLoader = ImageLoader.getInstance(view.context, CACHE_SIZE)
        imageLoader.displayImage(url,view, R.drawable.placeholder)
    }

}
