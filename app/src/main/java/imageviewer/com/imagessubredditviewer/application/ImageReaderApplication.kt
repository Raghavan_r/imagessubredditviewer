package imageviewer.com.imagessubredditviewer.application

import android.app.Application
import imageviewer.com.imagessubredditviewer.di.imageReaderApp
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level

class ImageReaderApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        stopKoin()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@ImageReaderApplication)
            modules(imageReaderApp)
        }
    }
}