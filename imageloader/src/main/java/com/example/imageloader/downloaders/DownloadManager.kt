package com.example.imageloader.downloaders

import java.util.concurrent.Callable

abstract class DownloadManager<T> : Callable<T> {
    abstract fun download(url: String): T
}