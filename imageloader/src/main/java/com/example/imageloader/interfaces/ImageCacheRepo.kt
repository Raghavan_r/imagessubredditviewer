package com.example.imageloader.interfaces

import android.graphics.Bitmap

interface ImageCacheRepo {
    fun put(url: String, bitmap: Bitmap)
    fun get(url: String): Bitmap?
    fun clear()
}