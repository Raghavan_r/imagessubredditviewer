package com.example.imageloader.main

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import com.example.imageloader.cache.CacheRepo
import com.example.imageloader.config.Configuration
import com.example.imageloader.downloaders.DownloadImage
import com.example.imageloader.downloaders.DownloadManager
import java.util.*
import java.util.Collections.synchronizedMap
import java.util.concurrent.Executors
import java.util.concurrent.Future
import kotlin.collections.HashMap


class ImageLoader (context: Context, cacheSize: Int) {
    private val cache = CacheRepo(context, cacheSize)
    private val executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
    private val mRunningDownloadList:HashMap<String,Future<Bitmap?>> = hashMapOf()
    private val imageViewMap = synchronizedMap(WeakHashMap<ImageView, String>())

    fun displayImage(url: String, imageView: ImageView, placeholder: Int) {
        val bitmap = cache.get(url)
        imageViewMap[imageView] = url
        bitmap?.let {
            imageView.setImageBitmap((it))
            return
        }
                ?: run {
                    imageView.tag = url
                    imageView.setImageResource(placeholder)
                    addDownloadImageTask( url, DownloadImage(url , imageView , cache)) }

    }

    private fun addDownloadImageTask(url: String, downloadTask: DownloadManager<Bitmap?>) {
        mRunningDownloadList.put(url,executorService.submit(downloadTask))
    }


    private fun clearcache() {
        cache.clear()
    }

    private fun cancelTask(url: String){
        synchronized(this){
            mRunningDownloadList.forEach {
                if (it.key == url &&  !it.value.isDone)
                    it.value.cancel(true)
            }
        }
    }

    private fun  cancelAll() {
        synchronized (this) {
            mRunningDownloadList.forEach{
                if ( !it.value.isDone)
                    it.value.cancel(true)
            }
            mRunningDownloadList.clear()
        }
    }

    companion object {
        private val INSTANCE: ImageLoader? = null
        @Synchronized
        fun getInstance(context: Context, cacheSize: Int = Configuration.defaultCacheSize): ImageLoader {
            return INSTANCE?.let { return INSTANCE }
                    ?: run {
                        return ImageLoader(context, cacheSize)
                    }
        }
    }

}