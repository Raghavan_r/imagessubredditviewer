package com.example.imageloader.cache

import android.graphics.Bitmap
import com.example.imageloader.interfaces.ImageCacheRepo
import android.util.LruCache
import com.example.imageloader.config.Configuration

class CacheMemory (newMaxSize: Int) : ImageCacheRepo{

    private val cache : LruCache<String, Bitmap>

    init {
        val cacheSize : Int
        if (newMaxSize > Configuration.maxMemory) {
            cacheSize = Configuration.defaultCacheSize
        } else {
            cacheSize = newMaxSize
        }
        cache = object : LruCache<String, Bitmap>(cacheSize) {
            override fun sizeOf(key: String, value: Bitmap): Int {
                return (value.rowBytes)*(value.height)/1024
            }
        }
    }

    override fun put(url: String, bitmap: Bitmap) {
        cache.put(url,bitmap)
    }

    override fun get(url: String): Bitmap? {
        return  cache.get(url)
    }

    override fun clear() {
        cache.evictAll()
    }


}