package com.example.imageloader.cache

import android.content.Context
import android.graphics.Bitmap
import com.example.imageloader.interfaces.ImageCacheRepo

class CacheRepo (context: Context, newMaxSize: Int ): ImageCacheRepo{

    val diskCache = DiskCache.getInstance(context)
    val memoryCache = CacheMemory(newMaxSize)

    override fun put(url: String, bitmap: Bitmap) {
        memoryCache.put(url,bitmap)
        diskCache.put(url,bitmap)
    }

    override fun get(url: String): Bitmap? {
        return memoryCache.get(url)?:diskCache.get(url)
    }

    override fun clear() {
        memoryCache.clear()
        diskCache.clear()
    }
}