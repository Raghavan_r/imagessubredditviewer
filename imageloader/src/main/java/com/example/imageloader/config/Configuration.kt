package com.example.imageloader.config

class Configuration {
    companion object {
        val maxMemory = Runtime.getRuntime().maxMemory() /1024
        val defaultCacheSize = (maxMemory/2).toInt()
    }
}